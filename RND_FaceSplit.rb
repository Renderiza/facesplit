=begin
(c) 2013, Anar | Dan Rathbun | Pilou | Renderiza | TIG

Permission to use, copy, modify, and distribute this software for
any purpose and without fee is hereby granted, provided the above
copyright notice appear in all copies.

THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

== Information
Author:: Anar | Dan Rathbun | Pilou | Renderiza | TIG
Name:: FaceSplit
Version:: 1.0.3
SU Version:: 2013
Date:: 11/4/2013
Description:: Split faces up a variety of ways.
=end

module RND_Extensions
	#
	module RND_FaceSplit
		require 'sketchup.rb'
		require 'extensions.rb'
		#
		unless file_loaded?( __FILE__ )
			file_loaded( __FILE__ )
			# Create the extension.
			ext = SketchupExtension.new 'FaceSplit', 'RND_FaceSplit/RND_FaceSplit_menus.rb'
			#
			# Attach some nice info.
			ext.creator     = 'Anar | Dan Rathbun | Pilou | Renderiza | TIG'
			ext.version     = '1.0.3'
			ext.copyright   = '2013, Anar | Dan Rathbun | Pilou | Renderiza | TIG'
			ext.description = 'Split faces up a variety of ways.'
			#
			# Register and load the extension on startup.
			Sketchup.register_extension ext, true
		end
		#
	end
	#
end
#
file_loaded( __FILE__ )