=begin
(c) 2013, Anar | Dan Rathbun | Pilou | Renderiza | TIG

Permission to use, copy, modify, and distribute this software for
any purpose and without fee is hereby granted, provided the above
copyright notice appear in all copies.

THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

== Information
Author:: Anar | Dan Rathbun | Pilou | Renderiza | TIG
Name:: FaceSplit
Version:: 1.0.3
SU Version:: 2013
Date:: 11/4/2013
Description:: Split faces up a variety of ways.
=end

module RND_Extensions
	#
	module RND_FaceSplit
		require 'sketchup.rb'
		require 'RND_FaceSplit/RND_FaceSplit_loader.rb'
	end
	#
	if !file_loaded?('rnd_menu_loader')
		@@rnd_tools_menu = UI.menu("Plugins").add_submenu("Renderiza Tools")
	end
	#
	#------New menu Items---------------------------
	if !file_loaded?('rnd_ew_loader')
		@@rnd_ew_menu = @@rnd_tools_menu.add_submenu("Websites")
		@@rnd_ew_menu.add_separator
		@@rnd_ew_menu.add_item("PluginStore"){UI.openURL('http://sketchucation.com/resources/pluginstore?pauthor=renderiza')}
		@@rnd_ew_menu.add_item("Extension Warehouse"){UI.openURL('http://extensions.sketchup.com/en/user/5451/store')}
		@@rnd_tools_menu.add_separator
		@@rnd_ew_menu.add_separator
	end
	#------------------------------------------------
	if !file_loaded?(__FILE__) then
		@@rnd_tools_menu.add_item('FaceSplit'){
		Sketchup.active_model.select_tool RND_Extensions::RND_FaceSplit::FaceSplit.new
		}
		# Add toolbar
		rnd_facesplit_tb = UI::Toolbar.new "FaceSplit"
		rnd_facesplit_cmd = UI::Command.new("FaceSplit"){
		Sketchup.active_model.select_tool RND_Extensions::RND_FaceSplit::FaceSplit.new
		}
		rnd_facesplit_cmd.small_icon = "img/rnd_facesplit_1_16.png"
		rnd_facesplit_cmd.large_icon = "img/rnd_facesplit_1_24.png"
		rnd_facesplit_cmd.tooltip = "FaceSplit"
		rnd_facesplit_cmd.status_bar_text = "Split faces up"
		rnd_facesplit_cmd.menu_text = "FaceSplit"
		rnd_facesplit_tb = rnd_facesplit_tb.add_item rnd_facesplit_cmd
		rnd_facesplit_tb.show
	end
	#
	file_loaded('rnd_ew_loader')
	file_loaded('rnd_menu_loader')
	file_loaded(__FILE__)
end